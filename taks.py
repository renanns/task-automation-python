import pyautogui
import time

# pausa em segundo para cada comando executado.
pyautogui.PAUSE = 1

# abrir navegador do chrome
pyautogui.press("win")   
pyautogui.write("chrome")
pyautogui.press("enter")

# pesquisar no navegador o sistema que será efetuado a automação.
pyautogui.write("https://dlp.hashtagtreinamentos.com/python/intensivao/login")
pyautogui.hotkey('ctrl', 'v')
pyautogui.press("enter")

time.sleep(2)

# realizar login no sistema
pyautogui.click(x=1854, y=405) # input position
pyautogui.write("atomsfrontend@gmail.com")
pyautogui.press("tab")
pyautogui.write("123")
pyautogui.press("tab") # button
pyautogui.press("enter")

import pandas

# ler base de dados
table = pandas.read_csv("produtos.csv")

# cadastrar produto
for line in table.index:
    pyautogui.click(x=1969, y=294) # input position
    pyautogui.write(str(table.loc[line, "codigo"]))
    pyautogui.press("tab")
    pyautogui.write(str(table.loc[line, "marca"]))
    pyautogui.press("tab")
    pyautogui.write(str(table.loc[line, "tipo"]))
    pyautogui.press("tab")
    pyautogui.write(str(table.loc[line, "categoria"]))
    pyautogui.press("tab")
    pyautogui.write(str(table.loc[line, "preco_unitario"]))
    pyautogui.press("tab")
    pyautogui.write(str(table.loc[line, "custo"]))
    pyautogui.press("tab")
    pyautogui.write(str(table.loc[line, "obs"]))
    pyautogui.press("tab")
    pyautogui.press("enter")
    # scroll para voltar para o topo da tela
    pyautogui.scroll(4000)